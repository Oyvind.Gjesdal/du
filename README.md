# Digital utvikling UB (DU)

# Repo for systemoversikt og systemdrift ved DU-UB. 

Gruppe for digital utvikling ved Universitetsbiblioteket i Bergen.

#### Begreper:
Systemeier: https://internkontroll-infosikkerhet.difi.no/begrepsliste-systemeier  
Systemforvalter: https://internkontroll-infosikkerhet.difi.no/begrepsliste-systemforvalter (ikke brukt her)  
Tjenesteier: Brukes her for den som eier selve tjenesten, funksjonen, ikke nødvendigvis den samme som eier systemet. 

# Systemer:



## 1. MerMEId


 | URL    | System | Språk | Host  | Tjenesteeier | Systemeier | Personer | Repo | 
 | ------ | ------ | ----- | ----- |-------------  | ---------- | -------- | ---- | 
 | http://ariel.uib.no/editor/ | MerMEId | XML MEI | vmWare | KMD (GA) | UB (DU) | Øyvind, Tarje, Ahl Vegard | GitHub  | 
 
MerMEId er et system for redigering, håndtering  og forhåndsvisning av musikk-metadata, basert på MEI (Music Encoding Initiative) XML-skjema.

#### Lenker til Repo:
https://github.com/ubbdst/MerMEId 

#### Type system: 
Open Source, utviklingspartner

#### Samarbeidspartnere/personer: 
Kirstine Folman UB (FS), Alex Geertinger, KB Danmark (utvikler), Arnulf Mattes (GA)

#### Integrasjoner:
*  Har utviklet JSON API som GRG benytter.

#### Utfordringer:
*  MerMEId oppdateres for sjelden. 
*  Konto for innlogging lages rett på server (trenger dokumentasjon)
*  Exist-DB forkaster tomcat i nyeste utgave av exist-db. Kanskje bidra/hjelpe MerMEId med flytt til en jetty-basert workflow? 

#### Løsningsforslag:
*  Beholde og redesigne
*  Planlegg oppgradering

#### Oppgaver:
*  Bygge ansible
*  Deploye til uh-skyen
*  https
*  ExistDB -> Tatt vekk støtte for Tomcat, som er MerMEIds 
*  Bidra med kode til KB


## 2. Avhandlingsportalen (BYOTh + GrOIN) 

 | URL    | Frontend | Backend | Database | Språk | Host | Tjenesteeier | Systemeier | Personer | Repo   |
 | ------ | -------- | ------  | -------- | ----- | ---- | ------------- | ---------- | -------- | -----  | 
 |  https://avhandling.uib.no | Angular | Express | Fedora (B-repo),  Redis | javascript | Azure | UB (FS)  og FA | UB (DU) | Ahl Vegard,  Hemed,  Tarje, Paul | GitLab/GitApp | 

System for innlevering av doktorgradsavhandlinger fra UiB til trykking, lagring, tilgjengeliggjøring, plagiatkontroll, samt levering av pressemedlinger og kalenderoppføringer til w3.
Presentasjon: https://slides.com/simonmitternacht/byoth#/  

#### Lenker til Repo:
https://git.app.uib.no/uib-ub/phd-portal

#### Type system: 
Open Source, egenutviklet 

#### Samarbeidspartnere/personer: 
Cato Kolsås ITA (utvikler B-repo, BORA), Are Johannessen ITA (utvikler w3), Lennart Nordgreen ITA (drift)

#### Staus:
*  Risikovurdert v-2019
*  Fra prosjekt til drift
*  Noen hovedleveranser fra prosjektet gjenstår

#### Integrasjoner:
*  W3
*  BORA
*  B-repo
*  Trykkeri
*  Plagiat
*  (Sak/arkiv)
*  (NB)

#### Utfordringer:
*  Egenkomponert løsning der viktige verktøy som errorlogging fra klienter og serverlogger først har kommet på plass etter hovedutvikler har sluttet.
*  Mange dependencies, må konstant oppdateres med patches
*  Integrasjon med Besvarerelsesrepo (B-repo) som database kan ha vært uheldig. UiB bør vurdere alternative lagringsløsninger for forvaltningsdata, som betyr omskriving av backend api med mindra man beholder sin egen Fedora-instans.

#### Løsningsforslag:
*  Styrke med ekstra utvikler/ og eller styrke samarbeid med ITA
*  Vurdere alternativ database?
*  På sikt: Vurdere opp mot innleveringsløsninger for masteroppgaver (felles system)/ nasjonal løsning

#### Oppgaver:
*  Siste leveranser ferdig
*  Brukergrensesnitt Fedora
*  Følge opp backburner jevnlig


## 3. BOAP (Bergen Open Access Publishing)
 
 | URL    | System | Språk | Host  | Tjenesteeier | Systemeier | Personer | Repo   | 
 | ------ | ------ |------ | ----- | ------------- | ---------- | -------- | ------ | 
 |  http://boap.uib.no | OJS | php | vmWAre | UB (FS) | UB (DU) | Ahl Vegard, Øyvind,  Tormod (FS) | Git |

System for publisering av åpne tidsskrift.
 
#### Lenker til Repo: 

#### Type system:
Open Source   

#### Status:
*  Ansible test-prosjekt

#### Integrasjoner:
*  Sender metadata og DOI til Crossref

#### Utfordringer:
*  En del bugs i OJS
*  Vi klarer ikke å oppdatere raskt nok

#### Løsningsforslag:
*  Vurdere nasjonal løsning for drift av tidsskrift og serier

#### Oppgaver:
*  Oppgarderinger
*  Ad-hoc støtte til Tormod i PDF-generereing



## 4. BORA (Bergen Open Reserach Archive)
 
 
 | URL    | System | Språk |  Host  | Tjenesteeier | Systemeier | Personer | Repo   | 
 | ------ | ------ |------ | ------ | ------------- | ---------- | -------  | ------ | 
 |  http://bora.uib.no | DSpace | java? | vmWare  | UB (FS)| UB (DU) | Irene (FS), Hemed,  Øyvind, Ahl Vegard | Git | 

Åpent forskningsarkiv (masteroppgaver, avhandlinger, artikler mm.)

#### Lenker til Repo: 

#### Type system: 
Open Source  

#### Samarbeidspartnere/personer: 
Cato Kolås ITA (utvikler), Helge Opedal? (drift)

#### Status:
*  Risikovurdert h-2017
*  Behovsmelding sendt til BIBSYS Brage

#### Integrasjoner:
* Cristin -> Frode.uib.no -> Sword -> BORA. Fungerer denne? 
* Cristin -> Frode.uib.no -> Manuell prosess-> BORA. Tarje har laget en manual på overføring mellom disse arkivene (https://gitlab.com/ubbdev/dst-doc/blob/master/publisering/bora.md#flytt-fra-frode-til-bora) 
* Brepo -> Masteroppgaver til BORA. Cato Kolsås har laget en integrasjon 
* Avh-API -> BORA. Cato lager en integrasjon 

#### Utfordringer:
* Ikke brukerstyrt innlevering eller metadataredigering 
* Dårlige integrasjoner med andre system 
* Tung plattform, oppgraderinger er utfordrene 
* Lever på en utdatert Redhat 6 maskin 
* Utviklingen av nasjonalt arkiv er treg 
* Kan ikke bygge bora for øyeblikket 


#### Løsningsforslag:
* Få Bora i en stand der en kan bruke/bygge den igjen 
* Vurdere Brage i påvente av NVA 
* Se på andre løsninger 
* Oppgradere? (Dspace 7 ikke lansert)

### Oppgaver:
*  Vurdere nytt system



## 5. CLARINO B-senter
 
 | URL    | System | Host  | Tjenetseier/ Prosjekteier | Systemeier | Personer | Repo | 
 | ------ | ------ |------ | ------------- | ------     | ------   | ------   | 
 |  http://repo.clarino.uib.no | DSpace | vmWare | LLE | UB (DU) | Hemed, Rune, Øyvind,  Paul | Git |  

Repository språkdata.


#### Lenke til Repo:

#### Type system: 
Open Source

#### Samarbeidspartnere/personer: 
Koneraad De Smedt (LLE), LINDAT

#### Status:
*  Risikovurdert h-2019
*  Clarino+ søknad til forhandling hos NFR. Arbeidspakker tilknyttet DU (Rune, Hemed).

#### Integrasjoner:

*  CLARIN Virtual Language Observatory, https://vlo.clarin.eu/? 

#### Utfordringer:
* Dspace drift 


#### Løsningsforslag:
* Dataverse fyller samme rolle hos UiT og UB har nå Dataverse for UiB datasett, kan man slå sammen? 
* Har Dataverse lisens og tilgangskontroll som er tilstrekkelig god? 
* Har den EDUGAIN innlogging for EU-forskere? 
* Fortsette å drifte CLARINO

#### Oppgaver:
* Knyttet til Clarino+
* Ansible-oppsett ved oppgradering?



## 6. Digitalt

 | URL    | System | Språk |  Host  | Tjenesteeier | Systemeier | Personer |   
 | ------ | ------ |------ | ------ | ------------ | ---------- |----------|     
 |  https://digitalt.uib.no | DSpace | java? | vmWare?  | UB (SPES) | UB (DU)? | ? |   

Arkiv med samlinger fra Spesialsamlingene og annet materiale.

#### Løsningsforslag:
Innhold flyttes og system avvikles. Se i sammenheng med Marcus?



## 7. Frode

 | URL    | System | Språk |  Host  | Tjenesteeier | Systemeier | Personer |  
 | ------ | ------ |------ | ------ | -------------| ---------- |--------  | 
 | https://frode.uib.no/ | DSpace | java? | vmWare?  | UB (FS)| UB (DU)? |?|  

Mellomarkiv mellom Cristin og BORA. Se i sammenheng med BORA.

#### Løsningsforslag:
Innhold flyttes og system avvikles.



## 8. Grieg Reserach Guide (GRG)
 
 
 | URL    | Frontend | Backend | Database | Språk | Host  | Tjenesteeier | Systemeier | Personer | Repo | 
 | ------ | -------- |-------- | -------- | ------ |----- | ------------- | ---------  | -------- |----- |
 | http://grg.uib.no/home  | Angular | Loopback3 | MongoDB |? | vmWare | KMD (GA) | UB | Ahl Vegard, Tarje | GitLab |

Grieg Research Guide er en nettbasert guide inneholdende en kommentert bibliografi over litteratur om Grieg og en verkfortegnelse over Griegs verk med semantiske relasjoner mellom referanser, personer og verk.
 
#### Lenker Repo:
https://gitlab.com/ubbdev/grg-api  
https://gitlab.com/ubbdev/grg-frontend   
 
 
#### Type system: 
Open Source, egneutviklet

#### Samarbeidspartnere/personer: 
Kirstine Folman UB (FS), Arnulf Mattes, Grieg-senteret, Arvid Vollsnes, pensjonist, Robin Garen Aaberg, Knowit, utvikler, robin.garen@gmail.com, med som privatperson 

Grieg Research Guide er en webapplikasjon som lagrer artikler I en MongoDB via Loopback3 rammeverket. 
Disse artiklerne lenker til referanser som importeres fra Zotero og verks fra MerMEId. 
Koden ble forsøkt gjenbrukt I NorLaw, men innsatsen for å sette det opp og gjøre tilpassninger ble ansett for stor I forhold til merverdien utover å bare bruke Zoteros egne funksjoner 

#### Integrasjoner:
* MerMEId 
* Zotero 
* Dataporten

#### Utfordringer:
* Egenkomponert CMS med alle de sikkerhets- og oppdateringsproblemene det medfører 
* Mye kode for lite funksjonalitet 
* Editor som krever for mye når nye behov melder seg 

#### Løsningsforslag:
*  Headless CMS og static site generator, f.eks. Sanity.io og Gatsby.js
*  Samtaler med Robin indikerer at det kan komme mer avanserte funksjoner på toppen av MerMEId, de kan kanskje integreres I GRG (I ny versjon?) 

### Oppgaver:
*  uh-skyen?
*  Bygge Ansible?
*  Flytte til uh-skyen?
*  Avvente evt. kontakt


## 9. Holbergsskrifter
 
 | URL    | System | Host  | Tjenesteseier | Systemeier | Personer | Repo   |
 | ------ | ------ |------ | ------------- | ------     | ------   | ------ |
 | http://holbergsskrifter.no | XFT | uh-skyen | UiB, Det Danske Sprog- og Litteraturselskab | UB (DU) | Øyvind | Git | 

I denne tekstkritiske utgaven gjøres Holbergs samlede forfatterskap digitalt tilgjengelig for første gang. Tekstene er søkbare og lenket opp mot faksimiler av førsteutgavene, og er utstyrt med ord- og sakkommentarer, innledninger og tekstkritiske noter.

#### Eksterne samarbeidspartnere/personer: 
DSL, Eiliv Vinje, LLE

#### Status:
*  Git support ingen utvikling

#### Oppgaver:
*  https?
*  Overføre bilder fra ub.uib.no, hoste billedfiler i IIIF?



## 10. Hordanamn

| URL    | System | Host  | Tjenesteeier | Systemeier | Personer | Repo   |  
| ------ | ------ |------ | -------------| ---------- | ---------| -------| 
| ?      |    ?   |   ?   |    LLE |   UB (SPS)| Øyvind |      | 

#### Status/oppgaver:
*  Uh-sky
*  Første forsøk på ansible, satt opp manuelt


## 11. Marcus
 
 
 | URL    | Frontend | Backend | Editor | Host  | Funksjonseier | Systemeier | Personer | Repo   |
 | ------ | -------- |-------- | -----  |------ | ------------- | ---------- | -------- | ------ |
 | http://marcus.uib.no/home | LODspeakr. | Fuseki | Protégé | VmWare/ UH-laaS | UB (Spes?) | UB (DU) | Øyvind, Hemed,  Tarje, Marianne (SPES)? ||  

##### Lenker Repo:

#### Type system:
Open source, egenutviklet

### Status:
Videre utvikling evt. vurderer nytt system

#### Utfordringer: 
* Må ha egen/sentralisert løsning for distribuering, redigering og lagring av bildefiler og IIIF manifest 
* IIIF Image API for distribuering av enkelt filer 
* Kan ha statisk frontend, da dette kan forenkle og gjøre det billigere å distribuere nettsider, men usikkert hvordan oppdatering vil kunne skje. Må alle sider regenereres hver gang? 
* Gatsbyjs kan være et alternativ for inhouse arbeid med formidling 

#### Løsningsforslag: 
* Videreutvikle dagens løsning
* Dersom man går for ekstern løsning, kan man fremdeles lage webpresentasjon, ala nettustillinger I en egen løsning med integrasjoner til ekstern  
* Kombinere Primus med egen datamodell i f.eks. Sanity. Objektfokusert i Primus og relasjoner i Sanity? Lage egne inputfelt som kan søke etter SPES-objekt I Primus?  

#### Oppgaver:
*  Gruppe/prosjekt vurdere videre utvikling

## 12. Medieval Linked Open DATA (MeLOD)
 
 | URL    | Frontend | Backend | Host  | Tjenesteeier | Systemeier | Personer | Eksterne | Repo   |
 | ------ | -------- |-------- | ----- |-------------- | ---------- | -------  | ------   | ------ |
 | https://melod.uib.no | LODspeakr. | Fuseki, Elastcsearch | UH-laaS | Åslaug Ommundsen? | UB (DU) | Hemed, Øyvind, Tarje | | 

#### Samarbeidspartnere/ personer:
Åslaug Ommundsen (LLE), Gjert Kristoffersen, Målføresamlinga 

#### Type system:
Open source, egenutviklet

#### Status:
Marcus i v5

#### Utfordringee /løsningsforslag:
*  Se kommentaerer under Marcus, må sees i sammenheng med hva som skjer der
*  Målføresamlinga akulle inn I Marcus-v5, men nå lever datasettet uten hjem 

### Oppgvaer:
*  Se i sammenheng med vurdering av Marcus 

## 13. Menota CMS
 
 | URL    | System | Host  | Tjenesteeier | Systemeier | Personer | Repo   |
 | ------ | ------ |------ | ------------- | ------     | ------   | ------ |
 | https://menota.org/forside.xhtml | ? | ? | LLE | UB (DU) | Øyvind|  GitApp |

Ligger under Clarino. Arkiv for Nordiske middeladertekster.

#### Lenke til Repo:
https://git.app.uib.no/Menota 

#### Samarbeidspartnere/personer:
Odd Einar Haugen, LLE

#### Oppgaver:
*  Ansible, uh-sky
*  endringsønkser CMS cirka < 5 ganger per år
*  noe generering av word-filer fra xml

## 14. Menota Korpus

 | URL    | System | Host  | Tjenesteeier | Systemeier | Personer | Repo   |
 | ------ | ------ |------ | ------------- | ------     | ------   | ------ |
 | http://clarino.uib.no/menota.org | egenutviklet (Korpuskel) | INESS-klyngen | LLE | UB (DU) | Paul |  GitApp / SVN |

 

## 15. PhDOnTrack
 
 | URL    | System | Host  | Tjenesteeier | Systemeier | Personer |  
 | ------ | ------ |------ | ------------- | ------     | ------   | 
 | https://www.phdontrack.net/| Wordpress | vmWare? | UB mfl.  | ? Drift ITA UiB, teknisk støtte UB UiO | Tarje | 

PhDOnTrack er en nettressurs laget i samarbeid med andre UH-bibliotek. Den kjører på Wordpress. 

#### Samarbeidspartnere/personer:
Michael Grote UB (US), Andrea Gasparini, UBO 

#### Utfordringer: 
* Lever på utdatert RedHat server på vmWare-anlegget til ITA, pr. 30.08.2019 
* Ligger fortsatt på uib ip per 30.08.2019 
* Frontend og admin i ett system 

#### Løsningsforslag: 
* Overføre UiO
* Beholde Wordpress
* Bytte til headless CMS og bygge statisk nettside 
* Krever gjennom gang og migrering av alt innhold med involvering av styrings-/arbeidsgruppen 

### Oppgaver:
*  Avvente forespørsel
*  Server

## 16. Proxy-tjeneste

| URL    | Programvare | Host  | Tjenesteeier | Systemeier | Personer |  
 | ------ | ------ |------ | ------------- | ------     | ------   | 
 | http://pva.uib.no | ezproxy: http://www.oclc.org/ezproxy | ? | UB | UB (DU)? | Ahl Vegard |

Tjenesten, som krever pålogging, gjør det mulig å få tilgang til UBs lisensierte e-ressurser fra utenfor UiBs nettet for ansatte og studenter ved UiB. 
Bibliotekets hjemmeside er satt opp slik at tjenesten automatisk kalles opp når det er nødvendig for tilgang til e-ressursene utenfor campus. 

#### Utfordringer:
* Proxy-server er i dag satt opp til å\ spille sammen med programmer fra Exlibris (Primo, Metalib og SFX) i Uniportsamarbeidet.
* Vi må sette opp nytt biblioteksystem fra OCLC, slik at det mest mulig sømløst kan benytte proxy-server, om dette vil kreve endringer på proxy-server  må avklares.

#### Løsningsforslag/oppgaver:
* Det kommer nye release av software. Det er ikke nødvendig eller ønskelig løpende å holde seg til siste versjon, 
men det kan være nødvendig å oppgradere, dersom vi trenger funksjoner/muligheter som kun finnes i nyere  utgaver.
* Flyttes til ToS?


## 17. Skeivt arkiv formidling (Drupal)

| URL    | System | Host  | Tjenesteeier | Systemeier | Personer |     
| ------ | ------ |------ | -------------| -------    | -------  |    
| https://skeivtarkiv.no/ | Drupal | Webhotell.uib | UB (SKA) | UB (DU)? | Hemed |   
 
#### Samarbeidspartnere/personer:
Hannah Gillow Kloster UB (SA)

#### Løsningsforslag:
*  Headless CMS?

#### Oppgaver:
*  Avvente forespørsel

## 18. Skeivt arkiv katalog (Marcus)

 | URL    | Frontend | Backend | Editor | Host  | Tjenesteeier | Systemeier | Personer | Repo   |  
 | ------ | -------- |-------- | -----  |------ | ------------- | ---------- | -------- | ------ |  
 | http://katalog.skeivtarkiv.no/collections  | ? | ? | Protégé | Webhotall.uib | UB (SA) | UB (DU) | Hemed, Øyvind | ? |  

#### Status:
*  Arbeid på katalogen er neglisjert. Ikke oppdatere skjema i Protégé. Planen var at SA skulle bruke samme system som Marcus.

#### Løsningsforslag/oppgaver:
*  Se i sammenheng med utvikling Marcus
*  Implementere Arkivportalen (1 ukes arbeid?)

## 19. Stadnamn

| URL    | Frontend | Backend | Editor | Host  | Tjenesteeier | Systemeier | Personer | Repo   | 
| ------ | ------   |------ | ---------| ----- | -------------| ---------- |----------|--------|  
| ?      | Se Marcus? |     |          |       | UB (SA)      | UB (DU)?   | Øyvind   |      |

#### Type system:
*  Linked Data, RDF
*  IIIF bildeserver og presentasjon

#### Status:
Prototype

#### Oppgaver:
*  Etablere plattform


## 20. Søk & Skriv

 
 | URL    | System | Host  | Tjenesteseier | Systemeier | Personer | Eksterne | Repo   |
 | ------ | ------ |------ | ------------- | ------     | ------   | ------   | ------ |
 | http://sokogskriv.no/  | Wordpress | vmWare | UB mfl | UB (DU) | Tarje | https://git.app.uib.no/uib-ub/sok-og-skriv |
 
SokogSkriv.no er en eldre nettressurs laget I samarbeid med andre UH-bibliotek. Den kjører på Wordpress.  

### Samarbeidspartnere/personer:
Henry Langseth, UB (US)
Ingerid Straume, HVL
Alexander Miguel Jensen Sewe, HVL, Alexander.Miguel.Jensen.Sewe@hvl.no 

#### Utfordringer: 
* Lever på utdatert RedHat server på vmWare-anlegget til ITA, pr. 27.05.2019 
* Frontend og admin I ett system 
* Oppdateringer gjøres av ekstern konsulent ved HVL, Alexander Miguel Jensen Sewe, som har tilgangsporblemer 
* HVL har sagt at de ikke skal levere eksterne tjenester, fokuserer på interne oppgaver 

#### Løsningsforslag: 
* Beholde wordpress
* Bytte til headless CMS og bygge statisk nettside
* Krever gjennom gang og migrering av alt innhold med involvering av styrings-/arbeidsgruppen

#### Oppgaver:
*  Avventer tilbakemedling Søk & Skriv

## 21. Termwiki:

 | URL    | System | Host  | Tjenesteeier | Systemeier | Personer | Repo  |  
 | ------ | ------ |------ | ------------ | ------     | ------   |------ |
 | https://termwiki.oyvindg.no?   | ? | ? | UB (SPS) | UB (DU)? | Øyvind, Paul? |  |  
 

#### Samarbeidspartnere/personer:
Sindre Sørensen , sindre@sedb.no, Terminologi, Mediawiki med tilpassinger (SPS) 
Gisle Andersen, NHH, “Eier” deler av terminologi-prosjektet 

#### Type system:

#### Oppgaver:
*  Oversette terminiologi.no til Semantic mediawiki
*  Konvertering av data til wikikode
*  Ansible oppsett
*  Bestille domene



## 22. WAB - Wittgenstein Ontology Explorer- Nachlass semantics
 
 | URL    | System | Host  | Tjenesteseier | Systemeier | Personer | Repo   | 
 | ------ | ------ |------ | ------------- | ------     | ------   | ------   | 
 | http://wab.uib.no/sfb/ | XML, Elasticsearch| LLE | Filosofi? | UB (DU) | Hemed, Øyvind | Git | 
 
#### Samarbeidspartnere/personer:
Alois Pichler, Frederic Kettelhoit, LLE


## 23. WAB - Wittgenstein Archives (WS)- Nachlass transcriptions
 
| URL    | System | Host  | Tjenesteseier | Systemeier | Personer | Repo   | 
 | ------ | ------ |------ | ------------- | ------     | ------   | ------   | 
 | http://wab.uib.no/sfb/ | Elasticsearch | ? | Filosofi? | UB (DU) | Øyvind ?| Git | 

#### Samarbeidspartnere/personer:
Alois Pichler, Frederic Kettelhoit, LLE

#### Løsningsforslag/Oppgaver:
*  Import av XML, generering av bilder
*  Flytte til UH-sky?
*  Hoste billedfiler i IIIF?

# Prosjekt, prototyper & system med andre systemeiere:


## 24. Birgitta (Omeka-S)
 
 | URL    | System | Språk | Host  | Tjenesteeier | Systemeier | Personer | Repo   | 
 | ------ | ------ |------ | ----- | ------------- | ---------- | -------- | ------ | 
 | https://birgitta.oyvindg.no | Omeka S/ https://omeka.org/s/ | php | UH-laaS | ? | UB (DU) | Tarje, Øyvind | GitApp | 
 
#### Lenker til Repo:
*  https://git.app.uib.no/uib-ub/ansible-omeka
*  https://git.app.uib.no/revision

#### Type system: 
Open Source

#### Samarbeidspartnere/personer: 
Julia King, Laura Miles (Fremmedspråk, HF)

#### Status:
* Prototype
* Ansible i testmaskin

#### Utfordringer:
* Satt opp uten grundig evaluering, trenger en avklaring på videre drift 
* Har en løsere tilnærming til domain og range i datamodeller, som gjør at mennesker kan bryte strengere datamodeller 
* Trenger D3 visualisering av nettverksanalyser 
* Export av data til triplestore -> Sparql queries til Gephi -> OmekaS 
* Vil kreve designarbeid og videreutvikling av template/themes 

#### Fordeler:
*  Er basert på Linked data og kan dekke småskala prosjekt.
*  Multitennant, samme installasjon kan gjenbrukes av alle aktører dersom man velger et godt domene.

#### Løsningsforslag/oppgaver:
*  Avklare videre drift
*  Trenger domene
*  Bygge ansible?


## 25. COMEDI

 | URL    | System | Host  | Språk | Tjenesteseier | Systemeier | Personer | Repo   |
 | ------ | ------ |------ | ------------- | ------     | ------   | ------ |----|
 | http://clarino.uib.no/comedi | egenutviklet | INESS-klynge | Common Lisp, SQL, Javascript | LLE? | LLE | Paul | SVN | 
 
 Clarin CMDI-1.2 metadata-editor. En del av Clarino-prosjektet.

### Oppgaver:
*  Repo bør flyttes til Git
*  Ny maskinvare integrert i UH-IaaS-skyen
*  Videreutvikling i Clarino+## 24. Diplomatarium

#### Status:
* Ikke startet
* TEI-XML filer fra Oslo og faksimiler fra NB, sy sammen. Potensielt samarbeidsprosjekt med Arkivverket og Språksamlingene.


## 26. ELMCIP

| URL    | System | Host  | Tjenesteeier | Systemeier | Personer |  
| ------ | ------ |------ | -------------| ---------- | --------  |   
| https://elmcip.net | Dupral | ? | LLE | LLE? | Aud |  
 
#### Samarbeidspersoner/personer:
Scott Rettberg, Stein Magne Bjørklund (LLE) 

#### Status UB:
*  Pilot

#### Oppgaver:

*  Prosjekt registering, metadata
*  Fremtidlig utvikling: Autoriteskontroll, overføring til Oria 

## 27. INESS

| URL    | System | Host  | Språk | Tjenesteseier | Systemeier | Personer | Repo   |
 | ------ | ------ |------ | ------------- | ------     | ------   | ------ |----|
 | http://clarino.uib.no/iness | egenutviklet | INESS-klynge | Common Lisp, SQL, Javascript | LLE? | LLE | Paul | SVN | 
 
 Infrastruktur for utforskning av syntaks og semantikk. En del av Clarino-prosjektet.

####Samarbeidspartnere/personer:
Sindre Sørensen

### Oppgaver:
*  Repo bør flyttes til Git
*  Ny maskinvare integrert i UH-IaaS-skyen
*  Videreutvikling i Clarino+

## 28. Korpuskel

 | URL    | System | Host  | Språk | Tjenesteseier | Systemeier | Personer | Repo   |
 | ------ | ------ |------ | ------------- | ------     | ------   | ------ |----|
 | http://clarino.uib.no/korpuskel | egenutviklet | INESS-klynge | Common Lisp, SQL, Javascript | LLE? | LLE | Paul | SVN | 
 
 Korpusverktøy. En del av Clarino-prosjektet.

### Oppgaver:
*  Repo bør flyttes til Git
*  Ny maskinvare integrert i UH-IaaS-skyen
*  Videreutvikling i Clarino+


## 29. Lagring & LOR
Prosjektet Lagring og LOR, under Læringslab-programmet tar sikte på å etablere fellesløsninger for å gi bedre arbeidsflyt og bedre dekning av behovene for lagring, deling og gjenbruk av digitale læringsressurser og video ved UiB. I hovedsak er dette tenkt løst ved å anskaffe og tilby et LOR-system som fungerer som et digialt bibliotek for læringsobjekter, samt en videolagringsløsning (MAM).


## 30. Norlaw (Norwegian Law in Foreign Languages)

 | URL    | System | Host  | Tjenesteeier | Systemeier | Personer | 
 | ------ | ------ |------ | ------------- | ------     | ------   | 
 | https://www.zotero.org/groups/1881030/norwegian_law_in_foreign_languages | Zotero | Zotero | UB (JUR) | UB (JUR)? | Øyvind, Elen Elvebakk | 

Bibliografi norsk lov i fremmedspråk, editert av det Juridiske fakultetsbibliotek, UB

## 31. Oria-integrasjon?
Burde være på ToS?


## 32. Redigeringsgrensesnitt revisjonsprosjektet

 | URL    | System | Host  | Tjenesteseier | Systemeier | Personer | Repo   | 
 | ------ | ------ |------ | ------------- | ------     | ------   | ------   | 
 | http://clarino.uib.no/lex | egenutviklet | INESS-klynge; Oracle | UB (SPS) | UB (DU)? | Paul | SVN | 
 
## 33. Termportalen:

| URL    | System | Host  | Tjenesteeier | Systemeier | Personer | Repo   | 
 | ------ | ------ |------ | ------------- | ------     | ------   |------ |
 | https://oda.uib.no/app/term/aterm  | ? | ? | ? | ? | Øyvind |  |
 

## 34. WAB - Nachlass search
 
 | URL    | System | Host  | Tjenesteseier | Systemeier | Personer | Repo   | 
 | ------ | ------ |------ | ------------- | ------     | ------   | ------   | 
 | http://wittfind.cis.uni-muenchen.de/ |  | ? | ? | ? | Øyvind |  | 
 
#### Samarbeidspartnere/personer:
Alois Pichler, Frederic Kettelhoit, LLE


## 35. WAB - Wittgenstein Source
 
 | URL    | System | Host  | Tjenesteseier | Systemeier | Personer | Repo   | 
 | ------ | ------ |------ | ------------- | ------     | ------   | ------   | 
 | http://www.wittgensteinsource.org/ | | ? | ? | ? | Øyvind |  | 
 
#### Samarbeidspartnere/personer:
Alois Pichler, Frederic Kettelhoit, LLE



